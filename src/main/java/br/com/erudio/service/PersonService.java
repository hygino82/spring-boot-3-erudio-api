package br.com.erudio.service;

import java.util.logging.Logger;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.erudio.dto.PersonDTO;
import br.com.erudio.dto.PersonInsertDTO;
import br.com.erudio.model.Person;
import br.com.erudio.repository.PersonRepository;

@Service
public class PersonService {

	private Logger logger = Logger.getLogger(PersonService.class.getName());

	private final ModelMapper mapper;
	private final PersonRepository personRepository;

	public PersonService(ModelMapper mapper, PersonRepository personRepository) {
		this.mapper = mapper;
		this.personRepository = personRepository;
	}

	public PersonDTO findById(Long id) {

		logger.info("Finding one Person with id: " + id);
		Person entity = personRepository.findPersonById(id);

		return mapper.map(entity, PersonDTO.class);
	}

	public Page<PersonDTO> findAll(Pageable pageable) {
		logger.info("Finding all Persons");
		Page<Person> page = personRepository.findAll(pageable);

		return page.map(x -> mapper.map(x, PersonDTO.class));
	}

	public PersonDTO create(PersonInsertDTO dto) {
		logger.info("Creating one Person");
		Person entity = mapper.map(dto, Person.class);
		entity = personRepository.save(entity);

		return mapper.map(entity, PersonDTO.class);
	}

	public PersonDTO update(Long id, PersonInsertDTO dto) {
		logger.info("Updating Person with id: " + id);
		Person obj = personRepository.findPersonById(id);
		obj.setFirstName(dto.getFirstName());
		obj.setLastName(dto.getLastName());
		obj.setAddress(dto.getAddress());
		obj.setGender(dto.getGender());

		obj = personRepository.save(obj);
		return mapper.map(obj, PersonDTO.class);
	}

	public void delete(Long id) {
		logger.info("Deleted Person with id: " + id);
		Person obj = personRepository.getReferenceById(id);

		personRepository.delete(obj);
	}
}
