package br.com.erudio.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.erudio.dto.PersonDTO;
import br.com.erudio.dto.PersonInsertDTO;
import br.com.erudio.service.PersonService;

@RestController
@RequestMapping("/api/person")
public class PersonControler {

	private final PersonService personService;

	public PersonControler(PersonService personService) {
		this.personService = personService;
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PersonDTO findById(@PathVariable(value = "id") Long id) {
		return personService.findById(id);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<PersonDTO> findAll(Pageable pageable) {
		return personService.findAll(pageable);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public PersonDTO create(@RequestBody PersonInsertDTO personDto) {
		return personService.create(personDto);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public PersonDTO update(@RequestBody PersonInsertDTO insertDto, @PathVariable Long id) {
		return personService.update(id, insertDto);
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable Long id) {
		personService.delete(id);
	}
}
